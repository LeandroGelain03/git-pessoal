Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root 'carrinho#index'
  resources :carrinho, only: [:index, :edit, :new, :destroy, :create, :update]

  post "carrinho/:id/edit", to:"carrinho#update", as: "carrinho_edit"
  post "carrinho/:id/destroy", to:"carrinho#destroy", as: "carrinho_destroy"
  
end
